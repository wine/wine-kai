The Wine development release 1.1.8 is now available.

What's new in this release (see below for details):
  - Substantial parts of inetcomm implemented (for Outlook).
  - Still better crypt32 support.
  - Memory management improvements.
  - Theming support for buttons.
  - Various bug fixes.

The source is available from the following locations:

  http://ibiblio.org/pub/linux/system/emulators/wine/wine-1.1.8.tar.bz2
  http://prdownloads.sourceforge.net/wine/wine-1.1.8.tar.bz2

Binary packages for various distributions will be available from:

  http://www.winehq.org/site/download

You will find documentation on http://www.winehq.org/site/documentation

You can also get the current source directly from the git
repository. Check http://www.winehq.org/site/git for details.

Wine is available thanks to the work of many people. See the file
AUTHORS in the distribution for the complete list.

----------------------------------------------------------------

Bugs fixed in 1.1.8:

   3109  Werkkzeug 1 remains black and creates buffer underruns
   3493  Commandos get's very slow (almost locked) while mouse is moving
   3819  Keyboard problem with Alien vs Predator 2 demo
   3910  PortAudio error at Unable to open streams: Illegal error number
   4046  Freezes waiting for short sound-samples that don't happen with sound off.
   4063  Quitting webed causes abort in _CheckNotSysLevel
   4078  memory allocation fails
   4144  Numerous RichText problems
   4558  Some numeric keypad keys don't work in user mode
   4855  ZOC window does not retain size after workspace switch
   4977  Mindstar Script Editor aborts on startup with "Runtime Error 430: Class does not support Automation..."
   5101  Age of Mythology The Titans - Memory Leak
   5224  Grim Fandango patch Gfupd101.exe doesn't start
   5707  Settlers IV is slow in WIne 0.9.16+
   5784  Dark Age of Camelot not connect update server
   6683  IrfanView's "Save Picture as ..." dialog problems
   6694  Java App Problems with Filechooser
   6700  Magic Workstation Application occupying 100% of cpu
   6709  [Game] Ceasar IV crashes.
   6948  CSpy/Tab: Tab images have wrong offset
   7052  The game Locomotion crashes on startup (after drawing an empty desktop window) due to an unhandled page fault.
   7639  Papyrus loses focus when user clicks on font size pulldown
   8234  Supreme Commander Install fail - Error : 87
   9460  Sid Meiers Pirates does not load after caps changes
   9577  Serious Sam II demo installer creates blank window
   9714  MapViewOfFile with write permissions should not succeed on a read-only file mapping
   9715  winecfg should allow display resolution beyond 120dpi
  10129  Guitar Rig 3 crashes
  10375  Stranger demo makes Wine segfaults
  10579  mingw32: reimp and other tools can't launch each other
  10727  .Net 2.0 does not install
  10810  Constant crash while using EDXOR
  10859  access violation in mshtml.dll since 0.9.48 / Heredis 9
  10980  ConvertImage cannot run
  11092  cutscene does not display correctly - jedi knight dark forces II demo
  11199  Unhandled exception: page fault on read access to 0x00000050 when starting Archlord
  11369  CheckPoint SmartDashboard R65 causes wine crash
  11483  MSTSC (remote desktop) needs winscard.dll.SCardAccessStartedEvent
  12046  MinGW32: unhandled page faults and exceptions
  12175  Blank window appears during Sims 2 installation
  12219  The "House of the Dead 3" "start game" options menu doesn't show any text.
  12244  Unhandled page fault in Lotus Wordpro R9.5
  12282  Oracle Forms 6i runtime crashes while opening a form
  12545  The Ship: Does not launch
  12584  Rendering problems in NWN2
  12669  Flash 8: crashes when a pixmap in clipboard
  12711  Guitar Pro 5 don't show in the task bar
  12864  Quick 2007 Home & Business fails during startup
  12865  Flash wIndow initially displays off screen
  12940  e-sword app does not install
  12953  DAZStudio 2.1 installer intermittantly hangs on startup
  12974  Crash of Max Payne v1 on radeon DRI
  12986  emachineshop crashes when closing the help window
  13026  winetest may start without a tag
  13103  tomtom home does not install due to no loading sensapi.dll
  13135  Wine crash when starting program hedgewars
  13255  sigabort for most files used with vissim 7.0
  13285  WriteIniValues does not create c:/windows/DieVölkerGold.ini
  13312  shipsim 2006 crashes
  13329  World of Warcraft (WoW) trial installer crashes with unimplemented function js3250.dll.JS_SetGCParameter
  13421  Lotus Notes 6.5.4 - wrong windows behavior
  13546  Che Guevara demo crashes
  13557  Regression. mdac25 fails to install
  13672  mIRC 6.32 script editor crashes with assertion failure in riched20
  13737  Word 2003 crashes on opening specific file
  14037  worldwide telescope installer crashes in X11DRV_GetBitmapBits
  14333  WALL-E demo displays upside down.
  14437  Voipcheap installs but does not run
  14477  Installer of sins of solar empire is not copying any files at all
  14499  ImageDirectoryEntryToDataEx: section header param [out, optional], needs to be zeroed before RtlImageRvaToVa
  14560  Australian etax program help feature segfaults
  14658  Font rendering regression
  14731  Crash upon starting a network server in 1000 game
  14751  Max Payne 2: PP effects produce black screen in ARB mode
  14774  Max Payne 2: PP effects flood console with GL errors (ORM=fbo)
  14817  Msi property names passed on the command line need to be interpreted as uppercase (Corel Draw X3 installer)
  14983  CM2000 Graphics slowed down !
  15142  Diablo - The Hell & Hellfire: DirectX issues
  15607  MyPhotoBooks doesn't run
  15691  Page fault while editing RichText
  15692  Apps crash with FontLink settings since Wine 1.1.6
  15722  OpenOffice3 apps won't run
  15725  calendar sizing incorrect in Paf5
  15756  Inline file rename text box does not disappear appropriately in 7-zip 4.60 beta
  15757  Address bar is not updated when navigating through directories in 7-zip 4.60 beta
  15758  Page Maker 6.5 can't find resource file
  15786  FEAR 1.08: GL errors in D3D8 mode
  15807  Problem editing HTML in TestLog
  15829  1000net installer crashes after selecting install button
  15842  winecfg: You don't have a drive C. This is not so great.
  15854  Age of Mythology: Vertex pipeline replacement patches break water transparency.
  15863  Unable to find X development files on Leopard
  15937  [PATCH] Pens of width = 1 scale on Windows, don't on Wine

----------------------------------------------------------------

Changes since 1.1.7:

Alexandre Julliard (45):
      jscript: Avoid a compiler warning.
      shdocvw/tests: Avoid sizeof in trace.
      ntdll: Make sure the last relocation contains some data.
      ntdll: Use the end of the reserved area as address space limit, in case we have more than 3Gb available.
      ntdll: Ignore some system directories in NtQueryDirectoryFile to avoid recursion troubles.
      user32: Move handling of internal messages into peek_message.
      user32: Move thread info setting and WH_GETMESSAGE call into peek_message.
      user32: Specify the new queue mask separately from the PeekMessage flags.
      user32: Use a local buffer in peek_message to save a server call for small buffer sizes.
      user32: Add a bunch of tests for GetQueueStatus and GetMessage combinations.
      ntdll: Simplify the RtlIsDosDeviceName_U implementation.
      ntdll: Add a few more tests for RtlIsDosDeviceName_U, fix some failures on Windows.
      ntdll: Allocate a new virtual region for large blocks, and ensure 16-byte alignment.
      ntdll: Store the exit code in the server in RtlExitUserThread.
      kernel32: Only try to open a VxD if opening a normal device failed.
      kernel32: Use ntdll functions where possible to implement the pthread wrappers.
      kernel32: Moved the pthread emulation support to ntdll.
      configure: Re-generate with autoconf 2.63.
      ntdll: Merge HEAP_InitSubHeap and HEAP_CreateSubHeap.
      ntdll: Create a separate heap for allocating memory views instead of using malloc.
      kernel32: Allocate global arenas on the Win32 heap.
      kernel32: Call build_argv in the parent process so that it can use the Win32 heap.
      kernel32: Call build_envp in the parent process so that it can use the Win32 heap.
      winecfg: Display an nicer error when the mount manager cannot be accessed.
      ntdll: Initialize the large address space before attaching dlls.
      ntdll: Return more correct information for SystemBasicInformation and GetSystemInfo.
      winemenubuilder: Move the xdg global variables out of the libpng ifdef block.
      wrc: Fix the duplicate resource check for user-defined types.
      ntdll: Store the per-view flags in the high word of the page protection bits.
      ntdll: Add a noexec flag for memory views where we don't want to force exec permission.
      kernel32/tests: Fix a couple of failures on Windows.
      kernel32/tests: Add a few more tests, fix some failures on Windows.
      ntdll: Store the per-page committed status in the server for anonymous file mappings.
      ntdll: Don't force anonymous file mappings to always be fully committed.
      ntdll/tests: Fix a couple of tests on NT4.
      ntdll,server: Fixed access checks for OpenFileMapping and MapViewOfFile.
      include: Add extern "C" to the exported Wine headers.
      kernel32/tests: Fix a few more errors on Win9x.
      ntdll: Keep track of the current working set limit independently of system views that may be allocated beyond it.
      ntdll: Do not report non-reserved memory areas as free since we don't know what's in them.
      kernel32/tests: Fix one more failing test on Win9x.
      ntdll: Store correct values in the various limits on all platforms instead of using 0.
      loader: Reserve some space for the virtual heap too.
      loader: Use a hidden function instead of an exported global variable to setup pthread functions.
      advapi32/tests: Avoid sizeof in traces.

Alistair Leslie-Hughes (6):
      comdlg32: Fix test under win98.
      msxml3: Add support to get_nodeTypedValue on Element Nodes.
      oleacc: Removed failed test case.
      inetcomm: Correct test under w2k8.
      mshtml: Supply a default value for get_designMode.
      mshtml: Implement IHTMLElement get_document.

Andrew Fenn (1):
      xinput: Added header file that defines a lot of the functionality of the library.

Andrew Nguyen (1):
      winmm: Fix a potential infinite recursion bug.

Andrew Talbot (7):
      odbc32: Sign-compare warnings fix.
      oleaut32: Sign-compare warnings fix.
      oledlg: Sign-compare warnings fix.
      quartz: Sign-compare warnings fix.
      quartz: Sign-compare warnings fix.
      riched20: Sign-compare warnings fix.
      rpcrt4: Sign-compare warnings fix.

Aric Stewart (7):
      ws2_32: Only set the overlapped hEvent if it exists.
      crypt32: Static functions CertContext_GetProperty and CertContext_SetProperty do not need to be WINAPI.
      ntdll: Fix parsing of proc file. Also correct memory deallocation on the Mac.
      gdiplus: Stub implementation of GdipNewInstalledFontCollection.
      msimtf: Add a mostly wrapper stub for IActiveIMMApp.
      shell32: Do not automatically fail in SHGetFileInfo if the flag SHGFI_USEFILEATTRIBUTES is combined with SHGFI_ATTRIBUTES, SHGFI_EXETYPE, or SHGFI_PIDL.
      comctl32: Add implementation of LVS_EX_ONECLICKACTIVATE.

Austin English (5):
      ole32: Implement IEnumFORMATETC_Next_Proxy and IEnumFORMATETC_Next_Stub.
      winecfg: Set default windows version to XP.
      advapi32/tests: fix a couple failures on Vista.
      crypt32: fix a test failure on Free/PC-BSD.
      wininet: Change a couple fixme's to warn's.

Bobby Bingham (1):
      gdi32: Add support for the GGO_UNHINTED flag in GetGlyphOutline.

Clinton Stimpson (1):
      comctl32: Fix getting of min size of monthcal when changing font.

Damjan Jovanovic (1):
      winemenubuilder: Generate icons in winemenubuilder instead of wineshelllink.

David Adam (3):
      d3dx8: Implement ID3DXMatrixStack_GetTop().
      d3dx8: Implement ID3DXMatrixStack_LoadIdentity.
      d3dx8: Implement ID3DXMatrixStack_LoadMatrix.

Detlef Riekenberg (3):
      user32/tests: Avoid crash on win9x.
      shdocvw/tests: Use the correct size for memset.
      user32/tests: Fix crash on win9x in the dde test.

Dmitry Timoshkov (9):
      gdi32: Search the child font list first.
      winex11.drv: Print the characters returned by ToUnicode.
      user32: Make ToAscii(Ex) and ToUnicode(Ex) prototypes match PSDK.
      wineconsole: Don't ignore extended keys.
      kernel32: Don't ignore extended keys.
      gdi32: Move DC mapping APIs to the 'dc' debug channel.
      user32: Handle all kinds of values returned by WIN_GetPtr.
      gdi32: Try to avoid not necessary glyph transformations.
      gdi32: Fix a broken test.

Dylan Smith (3):
      richedit: Added riched32 tests for word wrap.
      richedit: Fixed initial word wrap setting when emulating 1.0.
      richedit: Handle negative position given to EM_POSFROMCHAR.

Eric Pouech (1):
      winedbg: Add a kill command to kill the current process.

Francois Gouget (4):
      wined3d: Fix the WineDirect3DCreate() export.
      cryptui: Fix compilation on systems that don't support nameless unions.
      d3dx8/tests: Fix compilation on systems that don't support nameless unions.
      shlwapi/tests: AssocQueryStringA/W() are missing on Windows 98.

Frans Kool (1):
      oleacc: Added Dutch translations.

Hans Leidekker (14):
      wsock32: Make EnumProtocols a wrapper around WSAEnumProtocols instead of forwarding directly.
      inetcomm: Add a stub implementation of IPOP3Transport.
      inetcomm: Add a stub implementation of ISMTPTransport2.
      inetcomm: Add an implementation of the pop3 USER and PASS commands.
      inetcomm: Add an implementation of IPOP3Transport::CommandLIST.
      inetcomm: Add an implementation of IPOP3Transport::CommandQUIT.
      inetcomm: Add an implementation of IPOP3Transport::CommandSTAT.
      inetcomm: Add an implementation of IPOP3Transport::CommandUIDL.
      inetcomm: Implement IPOP3Transport::Disconnect.
      inetcomm: Add an implementation of IPOP3Transport::CommandUSER.
      inetcomm: Add an implementation of IPOP3Transport::CommandPASS.
      inetcomm: Add an implementation of ISMTPTransport2::CommandDATA.
      inetcomm: Advertise support for a couple more interfaces.
      inetcomm: CreateIMAPTransport, CreatePOP3Transport and CreateSMTPTransport are implemented.

Henri Verbeet (28):
      wined3d: Move depth_blt to surface.c.
      wined3d: Pass the texture type to the shader depth blt function.
      wined3d: Pass explicit texcoords to depth blt.
      wined3d: Support some more texture types for GLSL depth blts.
      dxgi: Make some functions static.
      d3d10: Make some functions static.
      wined3d: Rename CreateAdditionalSwapChain to CreateSwapChain.
      wined3d: Fix some indentation.
      wined3d: Support some more depth blt texture types for arb programs.
      wined3d: Support some more depth blt texture types in surface_depth_blt().
      wined3d: Handle lack of NPOT support for depth blts.
      wined3d: Handle projected cube textures.
      dxgi: Add some stubs for IDXGIAdapter.
      dxgi: Add some stubs for IDXGISwapChain.
      d3d10: Add a test to show d3d10 devices implement IDXGIDevice.
      d3dx8: Return E_NOTIMPL from ID3DXMatrixStack stubs.
      d3dx8: Correct some ID3DXMatrixStack prototypes.
      d3dx8: Initialize the matrix stack in D3DXCreateMatrixStack().
      d3dx8: Implement ID3DXMatrixStack_Push() and ID3DXMatrixStack_Pop().
      d3dx8: Make expect_mat a bit more useable.
      d3dx8: Add a few tests for ID3DXMatrixStack.
      wined3d: Properly break in get_argreg() (LLVM/Clang).
      wined3d: Print an error when drawStridedSlowVs() is called with 0 idxSize and non-NULL idxData (LLVM/Clang).
      wined3d: Remove a FIXME that doesn't apply anymore.
      d3d8: Don't ignore the GetCursorInfo() return value (LLVM/Clang).
      d3d9: Don't ignore the GetCursorInfo() return value (LLVM/Clang).
      wined3d: vertexshader should never be NULL in generate_param_reorder_function() (LLVM/Clang).
      wined3d: Remove another redundant NULL check (LLVM/Clang).

Hervé Chanal (2):
      shell32: A nicer icon for "Open folder".
      shell32: A nicer icon for "folder".

Huw Davies (4):
      ole32: Create the '\1Ole' stream.
      ole32: Try to load the '\1Ole' stream and create it if it doesn't exist.
      ole32: Implement IEnum*_Next marshallers.
      ole32: Call the object's GetClassID if it's running.

Jacek Caban (4):
      mshtml: Remove no longer used interfaces.
      jscript: Fixed SetScriptSite called before InitNew handling.
      jscript: Added SCRIPTSTATE_CONNECTED implementation.
      jscript: Added SCRIPTITEM_ISVISIBLE flag implementation.

James Hawkins (13):
      msi: Set the source path tests to "interactive" to avoid timing out on some machines.
      msi: Use the long file name in the WriteIniValues action.
      msi: Convert command line property names to uppercase.
      msi: Skip the leading period of the extension to be registered.
      msi: Do not reinstall an assembly that already exists in the global assembly cache.
      msi: Allow private properties from the AdminProperties property list.
      msi: Factor out the table insertion code.
      msi: Factor out the table insertion code.
      msi: Factor out the table insertion code.
      msi: Factor out the code to open a product key.
      msi: Factor out the code to open the features key.
      msi: Factor out the code to open the UserData features key.
      msi: Remove an unused registry function.

Jeff Zaroyko (1):
      ws2_32: Test WSAAccept optional callback parameter before trying to use it.

Juan Lang (35):
      cryptui: Don't crash if pImportSrc is NULL.
      cryptui: Add tests for CryptUIWizImport.
      cryptui: Improve parameter checking.
      cryptui: Move cert creation to a helper function.
      cryptui: Support importing certificate contexts.
      cryptui: Test the import destination of a couple more certs.
      cryptui: Choose appropriate destination store for a cert.
      crypt32: Fix a failing test on Windows.
      cryptui: Fix a failing test on Windows.
      cryptui: Fix destination store for self-signed certs.
      crypt32: Fix failing test.
      crypt32: Fix frequency with which chains are checked for cycles.
      crypt32: Don't neglect status to ignore on a Windows platform when a test is todo_wine.
      crypt32: Fix chain error status when a cert's issuer can't be found.
      crypt32: Fix some test failures on Win9x/NT4.
      advapi32: Print error if opening /dev/urandom fails, and update comment.
      crypt32: Fix test failures on older versions of Windows.
      crypt32: Fix typo.
      wintrust: Add stub for WVTAsn1SpcFinancialCriteriaInfoEncode.
      wintrust: Add tests for WVTAsn1SpcFinancialCriteriaInfoEncode.
      wintrust: Implement WVTAsn1SpcFinancialCriteriaInfoEncode.
      wintrust: Add stub for WVTAsn1SpcFinancialCriteriaInfoDecode.
      wintrust: Add tests for WVTAsn1SpcFinancialCriteriaInfoDecode.
      wintrust: Implement WVTAsn1SpcFinancialCriteriaInfoDecode.
      crypt32: Add missing ok calls.
      crypt32: Separate checking the tag of encoded bits from decoding the bits.
      crypt32: Add tests for CryptFormatObject.
      crypt32: Add base implementation of CryptFormatObject.
      crypt32: Make a global copy of crypt32's HINSTANCE.
      crypt32: Implement CryptFormatObject for szOID_AUTHORITY_KEY_IDENTIFIER2.
      crypt32: Implement CryptFormatObject for szOID_ENHANCED_KEY_USAGE.
      crypt32: Implement CryptFormatObject for szOID_BASIC_CONSTRAINTS2.
      crypt32: Implement CryptFormatObject for szOID_AUTHORITY_INFO_ACCESS.
      crypt32: Implement CryptFormatObject for szOID_CRL_DIST_POINTS.
      crypt32: Implement CryptFormatObject for SPC_FINANCIAL_CRITERIA_OBJID.

Katayama Hirofumi MZ (1):
      notepad: Don't save maximized size.

Kirill K. Smirnov (1):
      gdi32: Fix copy/paste typo.

Krzysztof Kotlenga (1):
      winex11: Make pens of width = 1 scalable.

Kusanagi Kouichi (1):
      winex11: Implement large data transfers.

Lei Zhang (7):
      comctl32: Get rid of DragDetect.
      comctl32: Don't notify if listview edit box contents have not changed.
      comctl32: Reset nEditLabelItem in LISTVIEW_EndEditLabelT.
      comctl32: Only update comboex edit box if the mask has CBEIF_TEXT set.
      ntdll: Skip deleted files in read_directory_getdents.
      wininet: Remove redundant variables.
      wininet: Handle HTTP 303 redirects.

Marcus Meissner (9):
      netapi32: Fixed buffer sizes to GetUserNameW and GetComputerNameW.
      programs: Fixed two RegEnumValue name lengths.
      oleview: Fixed size passed to LoadStringW.
      ntoskrnl.exe: Check irp for NULL consistently.
      advapi32: Removed redundant NULL check.
      msxml3: Fixed NULL ptr dereference possibilities (Coverity).
      msxml3: Fixed if nesting / ptr checking problems in xmlnode_transformNode.
      mshtml: Fixed reversed NULl check.
      wininet: Removed redundant NULL check (Coverity).

Michael Karcher (3):
      msxml3: IXMLCDATASection is not an element.
      msxml3: Clean up initialization.
      msxml3: Simplify IXMLDOMNodeMap::removeNamedItem.

Michael Stefaniuc (54):
      advapi32/tests: Use 0 instead of casting NULL to a handle of integer type.
      rsaenh: Use 0 instead of casting NULL to a handle of integer type.
      comctl32: Just use 0 instead of casting NULL twice to an integer type.
      comctl32: HGDIOBJ is interchangeable with other handle types so don't cast.
      user32: HGDIOBJ is interchangeable with other handle types; no casts are needed.
      gdi32/tests: HGDIOBJ is interchangeable with other handle types; no casts are needed.
      taskmgr: HGDIOBJ is interchangeable with other handle types; no casts are needed.
      cmdlgtst: HGDIOBJ is interchangeable with other handle types; no casts are needed.
      cmdlgtst: Use NULL instead of casting 0 to a pointer.
      comctl32: Remove superfluous casts as HANDLE to other handle types.
      comctl32: HANDLE/HGLOBAL are basically just void pointers. No casts to other pointers are needed.
      comdlg32: Don't cast the return value of GetProp() as it is a HANLE aka void pointer.
      gdi32: Don't cast NULL.
      winedump: Remove casts of void pointers to other pointer types.
      winex11.drv: ImmLockIMCC() returns a void pointer; no need to cast that.
      cabinet: Do not cast NULL.
      riched20: Do not cast NULL.
      riched20: Use MAKELPARAM instead of "(LPARAM) MAKELONG".
      winedbg: Use FIELD_OFFSET instead of reimplementing it.
      user32: Do not cast NULL.
      dmusic: Do not cast NULL.
      dnsapi: Transform two for loops into while loops.
      d3dx8: Do not cast NULL.
      netapi32/tests: Do not cast NULL.
      ole32: Do not cast NULL.
      oleview: Do not cast NULL.
      setupapi: Do not cast NULL.
      dmsynth: Do not cast NULL.
      winedos: Do not cast NULL.
      shell32: Do not cast NULL.
      dmloader: Do not cast NULL.
      oleaut32: Do not cast NULL.
      comctl32: Do not cast NULL.
      Remove the remaining casts of NULL.
      avifil32: Remove superfluous casts of void pointers.
      uxtheme: Remove superflous casts.
      comctl32/tests: Use MAKELPARAM instead of "(LPARAM) MAKELONG".
      wined3d: Use the integer variant of zero instead of casting "0.0".
      kernel32: Do not cast zero.
      riched20: Remove superflous casts.
      comctl32: Do not cast zero.
      user32: Do not cast zero.
      crypt32: Remove superfluous casts of void pointers.
      iphlpapi: Do not cast zero.
      user32: Remove superfluous casts of void pointers.
      fusion: Remove superfluous casts of void pointers.
      shell32: Use FIELD_OFFSET instead of hand coding its functionality.
      make_requests: Do not generate code that casts zero to a pointer.
      shell32: Remove superfluous casts; mostly of void pointers.
      ole32: Remove some superfluous casts of void pointers and zero.
      shlwapi: Remove superfluous casts of/to void pointers.
      comdlg32: Remove superfluous casts of void pointers to other pointer types.
      comctl32: Remove superfluous casts that crept in as well as some older ones.
      crypt32: Fix the aligning up to a DWORD_PTR boundary.

Nicolas Le Cam (7):
      msi/tests: Fix a failing test on all platforms up to and including win2k.
      wininet/tests: Fix a failing test on IE6.
      shell32/tests: Fix a test on several platforms.
      user32: Partially implement SystemParametersInfo(SPI_{GET/SET}FOREGROUNDLOCKTIMEOUT).
      advapi32/tests: test_enum_provider_types test cleanup.
      advapi32/tests: Run a test on more platforms.
      cryptui/tests: Fix test failures on Win2k and below.

Nikolay Sivov (3):
      gdiplus: Add a structure to header for easier navigation (by wrapper class).
      gdiplus: Added CachedBitmap calls.
      gdiplus: Added TRACE(..) for Pen calls.

Owen Rudge (1):
      appwiz.cpl: Use MS Shell Dlg instead of MS Sans Serif for dialog boxes.

Paul Bryan Roberts (5):
      ntdll: Avoid potential infinite loop.
      advapi32/tests: Simple tests of GetFileSecurity()/SetFileSecurity().
      advapi32: Add TRACE to GetFileSecurity().
      server: Refactor server side implementation of GetFileSecurity().
      server: Clone file_get_sd() and file_set_fd() for directories.

Paul Vriens (12):
      shlwapi/tests: Fix some failures on XP and W2K3.
      ddraw/tests: Fix a test on W2K3.
      shlwapi/tests: Fix a failure on Win9x and NT4.
      shell32/tests: Fix a test failure on NT4.
      rsaenh/tests: Fix a test failure on NT4 and below.
      user32/tests: Fix some test failures on Win9x and WinMe.
      winmm/tests: Fix failure on Win9x and WinMe.
      gdi32/tests: Don't crash on NT4.
      user32/tests: Fix some test failures on Win9x.
      user32/tests: Set last error if the menu item cannot be found.
      user32/tests: Skip GetMenuItemInfo tests on NT4 and below.
      winmm/tests: Fix a test failure on Vista and W2K8.

Reece Dunn (6):
      winecfg: Add ellipsis ('...') to buttons that launch other dialogs.
      winecfg: Renamed 'Shell Folder' to 'Folder'.
      uxtheme: Fixed the todo blocks in the IsThemed tests when theming is inactive.
      comctl32: Support themed push buttons.
      comctl32: Support themed check boxes and radio buttons.
      comctl32: Don't draw the theme background of the group box over it's content area.

Rob Shearman (9):
      inetcomm: Add an implementation of the HELO/EHLO command.
      inetcomm: Add an implementation of ISMTPTransport2::SendMessage.
      inetcomm: Add an implementation of ISMTPTransport2::CommandQUIT.
      inetcomm: Add an implementation of ISMTPTransport2::CommandMAIL.
      inetcomm: Add an implementation of ISMTPTransport2::CommandRCPT.
      inetcomm: Add an implementation of ISMTPTransport2::CommandEHLO.
      inetcomm: Add an implementation of ISMTPTransport2::CommandHELO.
      inetcomm: Add an implementation of ISMTPTransport2::CommandAUTH.
      inetcomm: Add an implementation of ISMTPTransport2::CommandRSET.

Roderick Colenbrander (4):
      winex11: Move all delayed GLX context creation code to create_glxcontext in order to prepare for WGL_ARB_create_context.
      Opengl32: Add defines for WGL_/GLX_ARB_create_context.
      opengl32: Mark some opengl3 tests as wine_todo.
      wnaspi32: Fix a buffer size regression.

Sergey Khodych (5):
      winex11: BitBlt returns TRUE when drawing outside of the clipping or visible region.
      comctl32: toolbar: Calculate a correct size for empty buttons with the BTNS_AUTOSIZE style.
      comctl32: toolbar: Improve text layout in TBSTYLE_LIST toolbars.
      comctl32: toolbar: Use a cx field for buttons in TOOLBAR_WrapToolbar.
      comctl32/tests: Fix typo in toolbar todo test.

Stefan Dösinger (8):
      wined3d: Restore the fragment replacement prog after depth_blt.
      d3d9: Test the effect of lighting on the result alpha.
      d3d: Do not restore the display mode in ddraw.
      d3d9: Use the correct AddRef and Release macros.
      d3d9: WINED3DSURFACE_DESC::MultiSampleType is not a DWORD.
      wined3d: Fix the num blend values -> type match.
      wined3d: Kill the GL_ATI_envmap_bumpmap code.
      d3d9: Add a test for GetTexture with no texture set.

Tobias Jakobi (1):
      wined3d: Fix typo in baseshader.c.

Tony Wasserka (2):
      d3dx9: Implement D3DXCreateSprite.
      d3dx9: Implement ID3DXSprite_Draw.

Vincent Povirk (5):
      shell32: Try to guess the working directory in the run dialog.
      setupapi/tests: Add test for ProfileItems directive.
      setupapi: Implement ProfileItems directive.
      setupapi: Add a matching CoUninitialize call.
      rundll32: Build with -mwindows.

Vitaliy Margolen (5):
      winmm: Support more joysticks.
      dxdiagn: Replace remaining FIXMEs with TRACEs.
      dxdiagn: Add pagefile size info.
      dxdiagn: Add windows path.
      dxdiagn: Add service pack version.

Vitaly Perov (2):
      netapi32: Add stub for NetShareGetInfo.
      netapi32: Add stub for NetShareAdd.

--
Alexandre Julliard
julliard@winehq.org
